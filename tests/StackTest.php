<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Stack;

class StackTest extends TestCase
{
    /** @var \App\Stack */
    private $stack;

    public function setUp(): void
    {
        parent::setUp();
        $this->stack = new Stack();
    }

    // Sunny Day Tests //

    public function testEmpty(): void
    {
        $this->assertTrue($this->stack->empty());

        $this->stack->push('data');
        $this->assertFalse($this->stack->empty());
    }

    public function testPop(): void
    {
        $this->stack->push('qa test');
        $this->stack->pop();
        $this->assertTrue($this->stack->empty());
    }

    public function testPush(): void
    {
        $data = 77;
        $this->stack->push($data);
        $this->assertEquals($data, $this->stack->top());
    }

    public function testTop(): void
    {
        $this->stack->push('first data');
        $this->stack->push('12.34');
        $data = 'latest data';
        $this->stack->push($data);
        $this->assertEquals($data, $this->stack->top());
    }

    // Stress Tests //
    // Not testing for Max Stack capacity, as it is not documented for SPL
    // Testing limits could also require modifying php.ini

    public function testPopEmptyStack(): void
    {
        try {
            $this->stack->pop();
        } catch (\RuntimeException $rex) {
            // Should likely use codes for i18n, but this is easier to read
            $this->assertEquals("Can't shift from an empty datastructure", $rex->getMessage());
        }
    }


    public function testTopOfEmptyStack(): void
    {
        try {
            $this->stack->top();
        } catch (\RuntimeException $rex) {
            $this->assertEquals("Can't peek at an empty datastructure", $rex->getMessage());
        }
    }
}

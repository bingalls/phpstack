# Stack
A simple PHP stack as a Proxy design pattern.

## Description
Stack uses SplStack internally for performance.
However, not all PHP installs include the SPL libraries.
It will be easy to modify this code to use native PHP arrays for those cases.

## Install
This native PHP code only requires PHP 7.2 with SPL.
Tested on PHP 7.4 on Mac OSX.
No install required, but you will set up PSR-4 autoloading & testing tools with

`composer install`

## Test
cd into project directory
* `composer validate`
* `./vendor/bin/phpstan analyse -l8 src/Stack.php`
* `./vendor/bin/phpcs --standard=PSR12 src/Stack.php`
* `./vendor/bin/phpunit`

You can also run static analysis against the unit tests

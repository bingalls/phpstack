<?php

namespace App;

/**
 * Stack demo
 */
class Stack
{
    /**
     * @var \SplQueue
     */
    private $stack;

    /**
     * Initialize your data structure here.
     */
    public function __construct()
    {
        $this->stack = new \SplQueue();
    }

    /**
     * Returns whether the queue is empty.
     * @return Boolean
     */
    public function empty(): bool
    {
        return $this->stack->isEmpty();
    }

    /**
     * SplQueue also supports pop(), but we limit to queue behavior
     * @return mixed
     * @see top
     */
    public function pop()
    {
        return $this->stack->shift();
    }

    /**
     * SplQueue also supports push(), but we limit to queue behavior
     * @param mixed $x
     * @return void
     */
    public function push($x): void
    {
        $this->stack->unshift($x);
    }

    /**
     * View top element of stack. Like pop(), but immutable
     * @return mixed
     * @see pop
     */
    public function top()
    {
        return $this->stack->bottom();
    }
}
